package utility_common_method;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_data_extractor {

	public static  ArrayList<String> Excel_data_reader(String filename, String sheet_name, String tcname) throws IOException {
		ArrayList<String> Arraydata= new ArrayList<String>();
		String project_dir = System.getProperty("user.dir");

		// step1 create the object of file input stream to locate the data file
		FileInputStream fis = new FileInputStream(project_dir + "\\Data_files\\"+filename+".xlsx");

		// step2 create the XSSFWorkbook object to open the excel file
		XSSFWorkbook wb = new XSSFWorkbook(fis);

		// step3 fetch the number of sheets available in the excel sheet
		int count = wb.getNumberOfSheets();

		// step 4 access the sheet as per the input sheet name
		for (int i = 0; i < count; i++) {
			String sheetName = wb.getSheetName(i);  //Fetch sheet name one by one 

			if (sheetName.equals(sheet_name)) {      //get inside the sheet name
				System.out.println(sheetName);
				XSSFSheet sheet = wb.getSheetAt(i);    //get inside the sheet
				Iterator<Row> row = sheet.iterator();
				row.next();
				while (row.hasNext()) {              //itrate till the data is available 
					Row datarow = row.next();      // fetch the test case name as we want to compare 
					String tc_name = datarow.getCell(0).getStringCellValue();      //fetch the data at zero index
					//System.out.println(tc_name);

					if (tc_name.equals(tcname)) {                   //compare the test case name
						Iterator<Cell> cellvalues = datarow.iterator();
						while (cellvalues.hasNext()) {
							String test_data =cellvalues.next().getStringCellValue();
							System.out.println(test_data);
							Arraydata.add(test_data);
				}
						
					}
				}
				break;
			}
		}
		//wb.close();
		return Arraydata;

		

	}
}
