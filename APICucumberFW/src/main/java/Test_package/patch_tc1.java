package Test_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import API_common_method.common_method_handle_API;
import Endpoints.patch_endpoint;
import Request_repository.patch_request_repository;
import io.restassured.path.json.JsonPath;
import utility_common_method.handle_api_logs;
import utility_common_method.handle_directory;

public class patch_tc1 extends common_method_handle_API {
	@Test
	
	public static void executor() throws IOException {
		File log_dir=handle_directory.create_log_directory();
		
		String requestBody = patch_request_repository.patch_request_tc1();

		String endpoint = patch_endpoint.patch_endpoint_tc1();
		for (int i = 0; i < 5; i++) {
			int statusCode = patch_statusCode(requestBody, endpoint);
			System.out.println(statusCode);
			if (statusCode == 200) {

				String responseBody = patch_responseBody(requestBody, endpoint);
				System.out.println(responseBody);
				handle_api_logs.evidence_creator(log_dir, "patch_tc1",endpoint,requestBody,responseBody);
				patch_tc1.validator(requestBody, responseBody);
				break;

			} else {
				System.out.println("expected statuscode is not found hence retrying");
			}
		}
	}

	public static void validator(String requestbody, String responsebody) {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		
		String res_job = jsp_res.getString("job");
		String res_createdate = jsp_res.getString("updatedAt");
		res_createdate = res_createdate.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		
		Assert.assertEquals(res_createdate, expecteddate);
	}

}
